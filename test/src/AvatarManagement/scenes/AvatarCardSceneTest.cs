namespace Quin.Test;

using Godot;
using Chickensoft.GoDotTest;
using System.Threading.Tasks;
using Shouldly;
using GodotTestDriver.Drivers;


public class AvatarCardSceneTest : SceneTestClass {
  public AvatarCardSceneTest(Node testScene) : base(testScene) { }

  private AvatarCardScene _scene = default!;

  private readonly Avatar _nymph = (Avatar)ResourceLoader.Load("res://test/fixtures/nymph/nymph.avatar.tres");

  [Test]
  public async Task TestShowsAvatarName() {
    _scene = await _fixture.LoadAndAddScene<AvatarCardScene>(AvatarCardScene.SceneResourcePath);
    _scene.Populate(_nymph);

    _scene.NameLabel.Text.ShouldBe(_nymph.Name);
  }

  [Test]
  public async Task TestGraysOutNotOwnedAvatars() {
    _scene = await _fixture.LoadAndAddScene<AvatarCardScene>(AvatarCardScene.SceneResourcePath);
    _scene.Populate(_nymph);

    _scene.GrayOverlay.Visible.ShouldBe(true);
  }

  [Test]
  public async Task TestDoesNotGrayOutOwnedAvatars() {
    _scene = await _fixture.LoadAndAddScene<AvatarCardScene>(AvatarCardScene.SceneResourcePath);
    var player = Player();

    player.AddCard(_nymph);
    player.UpgradeAvatar(_nymph);

    _scene.Populate(_nymph);
    _scene.GrayOverlay.Visible.ShouldBe(false);
  }

  [Test]
  public async Task TestShowsAvatarLevel() {
    _scene = await _fixture.LoadAndAddScene<AvatarCardScene>(AvatarCardScene.SceneResourcePath);
    var player = Player();

    player.AddCard(_nymph);
    player.UpgradeAvatar(_nymph);

    _scene.Populate(_nymph);

    _scene.LevelLabel.Text.ShouldBe("1");
    _scene.NextLevelLabel.Text.ShouldBe("2");
  }

  [Test] // if avatar is locked, the unlock button appears
  public async Task TestShowsUnlockButton() {
    _scene = await _fixture.LoadAndAddScene<AvatarCardScene>(AvatarCardScene.SceneResourcePath);
    _scene.Populate(_nymph);

    _scene.UnlockButton.Visible.ShouldBeTrue();
  }

  [Test] // if avatar is unlocked, the unlock button doesn't appear
  public async Task TestDoesNotShowUnlockButton() {
    _scene = await _fixture.LoadAndAddScene<AvatarCardScene>(AvatarCardScene.SceneResourcePath);
    var player = Player();

    player.AddCard(_nymph);
    player.UpgradeAvatar(_nymph);

    _scene.Populate(_nymph);

    _scene.UnlockButton.Visible.ShouldBeFalse();
  }

  [Test] // User can't click button if they don't have enough cards to unlock
  public async Task TestCanNotClickUnlockButton() {
    _scene = await _fixture.LoadAndAddScene<AvatarCardScene>(AvatarCardScene.SceneResourcePath);
    _scene.Populate(_nymph);
    var player = Player();

    _scene.UnlockButton.Disabled.ShouldBeTrue();
  }

  [Test] // User can unlock avatar
  public async Task TestUnlockAvatar() {
    _scene = await _fixture.LoadAndAddScene<AvatarCardScene>(AvatarCardScene.SceneResourcePath);
    var player = Player();

    player.AddCard(_nymph);

    _scene.Populate(_nymph);

    _scene.ProgressBarContainer.Visible.ShouldBeFalse();
    _scene.UpgradeContainer.Visible.ShouldBeTrue();

    _scene.CardsLabel.Visible.ShouldBeTrue();
    _scene.CardsLabel.Text.ShouldBe("1 / 1");

    var buttonDriver = new BaseButtonDriver<TextureButton>(() => _scene.UnlockButton);
    await buttonDriver.Press();

    player = Player();
    player.Avatar(_nymph).ShouldNotBeNull();
    _scene.GrayOverlay.Visible.ShouldBeFalse();

    _scene.ProgressBarContainer.Visible.ShouldBeTrue();
    _scene.UpgradeContainer.Visible.ShouldBeFalse();
  }

  [Test] // User that has a capped avatar sees the number of cards needed
  public async Task TestNeededCards() {
    _scene = await _fixture.LoadAndAddScene<AvatarCardScene>(AvatarCardScene.SceneResourcePath);
    var player = Player();
    player.AddCard(_nymph);
    player.UpgradeAvatar(_nymph);
    player.AddAvatarExperience(_nymph, 1000);

    _scene.Populate(_nymph);

    _scene.ProgressBarContainer.Visible.ShouldBeFalse();
    _scene.UpgradeContainer.Visible.ShouldBeTrue();

    _scene.CardsLabel.Visible.ShouldBeTrue();
    _scene.CardsLabel.Text.ShouldBe("0 / 5");
  }

  [Test] // User needs to add exp to avatar
  public async Task TestNeededExp() {
    _scene = await _fixture.LoadAndAddScene<AvatarCardScene>(AvatarCardScene.SceneResourcePath);
    var player = Player();
    player.AddCard(_nymph);
    player.UpgradeAvatar(_nymph);
    player.AddAvatarExperience(_nymph, 100);

    _scene.Populate(_nymph);

    _scene.ProgressBarContainer.Visible.ShouldBeTrue();
    _scene.UpgradeContainer.Visible.ShouldBeFalse();

    _scene.ExpProgressBar.MinValue.ShouldBe(0);
    _scene.ExpProgressBar.MaxValue.ShouldBe(1000);
    _scene.ExpProgressBar.Value.ShouldBe(100);
  }
}

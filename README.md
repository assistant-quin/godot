# Quin

## Install dotnet

```powershell
winget install dotnet-sdk-8

# if it says it's already installed, run
winget upgrade dotnet-sdk-8
```

## Install git and git lfs

```powershell
winget install -e --id Git.Git
winget install -e --id GitHub.GitLFS
```

Open git bash from the system menu.

```gitbash
git lfs install
```

## Install Godot

```powershell
dotnet tool install --global Chickensoft.GodotEnv
godotenv godot install 4.2.0
```

## Clone the repository

```bash
git clone git@gitlab.com:assistant-quin/godot.git
```

## First time opening the project in Godot

**DO NOT OPEN ANY FILE YET**

Open the project then, in `Editors -> Settings -> General -> Text Editor -> Behavior`, **uncheck** both `Auto Indent` and `Convert Indent on Save`.

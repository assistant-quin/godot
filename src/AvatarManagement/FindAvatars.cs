namespace Quin;

using Godot;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public partial class FindAvatars : Node {
  private static readonly string[] _searchFolders = { "res://test", "res://src" };
  private static readonly Regex _regex = new(@"^.*\.avatar\.\w+$");
  private static readonly string _resourceName = "Avatar";

  public static List<Avatar> Process(Environment environment) {
    var avatars = new List<Avatar>();

    foreach (var folder in _searchFolders) {
      Files.WalkDir(folder, true, (filePath) => {
        if (ResourceLoader.Exists(filePath, _resourceName) && _regex.IsMatch(filePath)) {
          var avatar = ResourceLoader.Load<Avatar>(filePath, _resourceName);

          if (avatar.RunEnvironments.HasFlag(environment)) { avatars!.Add(avatar); }
        }
      });
    }

    return avatars;
  }
}

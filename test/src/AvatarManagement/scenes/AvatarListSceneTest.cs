namespace Quin.Test;

using Godot;
using Chickensoft.GoDotTest;
using Shouldly;
using System.Threading.Tasks;

public class AvatarListSceneTest : SceneTestClass {
  public AvatarListSceneTest(Node node) : base(node) { }

  private AvatarListScene _scene = default!;

  private readonly Avatar _nymph = (Avatar)ResourceLoader.Load("res://test/fixtures/nymph/nymph.avatar.tres");

  [Test]
  public async Task TestDisplaysAvatarCards() {
    _scene = await _fixture.LoadAndAddScene<AvatarListScene>(AvatarListScene.SceneResourcePath);
    _scene.CardsContainer.GetChildCount(true).ShouldBe(Globals.Avatars.Value.Count);
    _scene.CardsContainer.GetChild<AvatarCardScene>(0).Avatar.ShouldBe(_nymph);
  }
}

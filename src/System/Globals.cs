namespace Quin;

using System.Collections.Generic;
using System;
using Godot;

public partial class Globals : Node {
  public static Lazy<List<Avatar>> Avatars { get; } = new Lazy<List<Avatar>>(
    () => FindAvatars.Process(RunEnvironment.Current)
  );

  public Player Player { get; } = Player.Instance;
}

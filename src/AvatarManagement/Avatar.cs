namespace Quin;

using Godot;

using Godot.Collections;

[GlobalClass]
public partial class Avatar : Resource {
  [Export] public string Uid { get; set; } = "";
  [Export] public string Name { get; set; } = "";
  [Export] public PackedScene? AvatarScene { get; set; } = default;
  [Export] public PackedScene? EnvironmentScene { get; set; } = default;
  [Export] public Environment RunEnvironments { get; set; } = 0;
  [Export] public Array<AvatarLevel> Levels { get; set; } = new();
}

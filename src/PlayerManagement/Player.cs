namespace Quin;

using System;
using System.Collections.Generic;

public partial class Player {
  private static readonly Lazy<Player> _lazy = new(() => new Player());
  public static Player Instance => _lazy.Value;


  public int Tokens { get; set; }
  public int Shards { get; set; }
  private Dictionary<string, int> Cards { get; set; } = new();
  private Dictionary<string, UnlockedAvatar> Avatars { get; set; } = new();

  public void Reset() {
    Tokens = 0;
    Shards = 0;
    Cards = new();
    Avatars = new();
  }
  public int AddCard(Avatar avatar) {
    Cards.TryGetValue(avatar.Uid, out var count);
    Cards[avatar.Uid] = count + 1;

    return count + 1;
  }

  public int CardsCount(Avatar avatar) {
    Cards.TryGetValue(avatar.Uid, out var count);
    return count;
  }

  public bool UpgradeAvatar(Avatar avatar) {
    Avatars.TryGetValue(avatar.Uid, out var unlockedAvatar);
    unlockedAvatar ??= new UnlockedAvatar();

    var neededCards = avatar.Levels[unlockedAvatar.Level + 1].Cards;

    if (CardsCount(avatar) < neededCards) { return false; }

    unlockedAvatar.Level += 1;
    Cards[avatar.Uid] -= neededCards;
    Avatars[avatar.Uid] = unlockedAvatar;

    return true;
  }

  public UnlockedAvatar? Avatar(Avatar avatar) {
    Avatars.TryGetValue(avatar.Uid, out var unlockedAvatar);
    return unlockedAvatar;
  }

  public void AddAvatarExperience(Avatar avatar, int exp) {
    if (!Avatars.TryGetValue(avatar.Uid, out var unlockedAvatar)) {
      throw new ArgumentException("Avatar has to be unlocked", avatar.Uid);
    }

    unlockedAvatar.Exp += exp;
  }
}

namespace Quin;

using Godot;

[GlobalClass]
public partial class AvatarLevel : Resource {
  [Export] public int Exp { get; set; } = 0;
  [Export] public int Cards { get; set; } = 0;
}

namespace Quin;

using System;
using Godot;

public class Files {
  public static void WalkDir(string path, bool recursive, Action<string> function) {
    using var dir = DirAccess.Open(path);

    if (dir != null) {
      dir.ListDirBegin();

      var fileName = dir.GetNext();

      while (fileName != "") {
        var filePath = path + "/" + fileName;

        if (dir.CurrentIsDir()) {
          if (recursive && !fileName.StartsWith('.')) {
            WalkDir(filePath, true, function);
          }
        }
        else {
          function(filePath);
        }

        fileName = dir.GetNext();
      }
    }
  }
}

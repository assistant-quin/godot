namespace Quin;

public class UnlockedAvatar {
  public int Level { get; set; }
  public int Exp { get; set; }
}

namespace Quin.Test;

using Chickensoft.GoDotTest;
using Godot;
using GodotTestDriver;

public class SceneTestClass : Quin.TestClass {
  public SceneTestClass(Node testScene) : base(testScene) { }

  protected Fixture _fixture = default!;

  [SetupAll] public void SetupFixture() => _fixture = new Fixture(TestScene.GetTree());
  [CleanupAll] public void CleanupFixture() => _fixture.Cleanup();

  public Player Player() => TestScene.GetNode<Globals>("/root/GlobalValues").Player;
}

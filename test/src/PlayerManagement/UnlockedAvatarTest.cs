namespace Quin.Test;

using Godot;
using Chickensoft.GoDotTest;
using Shouldly;

public class UnlockedAvatarTest : TestClass {
  public UnlockedAvatarTest(Node scene) : base(scene) { }

  private readonly UnlockedAvatar _unlockedAvatar = new();

  [Test]
  public void TestLevel() {
    _unlockedAvatar.Level += 1;
    _unlockedAvatar.Level.ShouldBe(1);
  }

  [Test]
  public void TestExperience() {
    _unlockedAvatar.Exp += 100;
    _unlockedAvatar.Exp.ShouldBe(100);
  }
}

namespace Quin;

using Godot;

[GlobalClass]
public partial class AvatarCardScene : Control {
  public static readonly string SceneResourcePath = "res://src/AvatarManagement/scenes/AvatarCard.tscn";

  public Avatar Avatar = default!;
  public Label NameLabel { get; private set; } = default!;
  public Label LevelLabel { get; private set; } = default!;
  public Label NextLevelLabel { get; private set; } = default!;
  public Label CardsLabel { get; private set; } = default!;

  public TextureButton UnlockButton { get; private set; } = default!;
  public TextureButton UpgradeButton { get; private set; } = default!;

  public ColorRect GrayOverlay { get; private set; } = default!;

  public Container ProgressBarContainer { get; private set; } = default!;
  public Container UpgradeContainer { get; private set; } = default!;

  public ProgressBar ExpProgressBar { get; private set; } = default!;

  private Player Player { get; set; } = default!;

  public override void _Ready() {
    Player = GetNode<Globals>("/root/GlobalValues").Player;

    NameLabel = GetNode<Label>("%NameLabel");
    LevelLabel = GetNode<Label>("%LevelLabel");
    NextLevelLabel = GetNode<Label>("%NextLevelLabel");
    CardsLabel = GetNode<Label>("%CardsLabel");

    ProgressBarContainer = GetNode<Container>("%ProgressBarContainer");
    UpgradeContainer = GetNode<Container>("%UpgradeContainer");

    UnlockButton = GetNode<TextureButton>("%UnlockButton");
    UnlockButton.Pressed += Unlock;

    GrayOverlay = GetNode<ColorRect>("%GrayOverlay");

    ExpProgressBar = GetNode<ProgressBar>("%ExpProgressBar");
  }

  public void Populate(Avatar avatar) {
    Avatar = avatar;

    var globals = GetNode<Globals>("/root/GlobalValues");
    Player = globals.Player;
    var playerAvatar = Player.Avatar(Avatar);

    if (playerAvatar != null) {
      GrayOverlay.Visible = false;
      UnlockButton.Visible = false;

      if (playerAvatar.Exp == Avatar.Levels[playerAvatar.Level].Exp) {
        UpgradeContainer.Visible = true;
        ProgressBarContainer.Visible = false;

        var playerCards = Player.CardsCount(Avatar);
        var nextLevelCards = Avatar.Levels[playerAvatar.Level + 1].Cards;
        CardsLabel.Text = $"{playerCards} / {nextLevelCards}";
      }
      else {
        UpgradeContainer.Visible = false;
        ProgressBarContainer.Visible = true;

        ExpProgressBar.MaxValue = Avatar.Levels[playerAvatar.Level].Exp;
        ExpProgressBar.Value = playerAvatar.Exp;
      }

      LevelLabel.Text = playerAvatar.Level.ToString();
      //if (playerAvatar.Exp < Avatar.)

      if (Avatar.Levels.Count > playerAvatar.Level) {
        NextLevelLabel.Text = (playerAvatar.Level + 1).ToString();
      }
    }
    else {
      GrayOverlay.Visible = true;
      UpgradeContainer.Visible = true;
      ProgressBarContainer.Visible = false;
      UnlockButton.Visible = true;

      UnlockButton.Disabled = Player.CardsCount(avatar) < avatar.Levels[1].Cards;

      var playerCards = Player.CardsCount(Avatar);
      var nextLevelCards = Avatar.Levels[1].Cards;
      CardsLabel.Text = $"{playerCards} / {nextLevelCards}";
    }

    NameLabel.Text = Avatar.Name;
  }

  public void Unlock() {
    if (Player.CardsCount(Avatar) < Avatar.Levels[1].Cards) {
      return;
    }

    Player.UpgradeAvatar(Avatar);
    UnlockButton.Pressed -= Unlock;
    Populate(Avatar);
  }
}

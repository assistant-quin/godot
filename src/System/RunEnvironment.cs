namespace Quin;

using Godot;
using System;

[Flags] public enum Environment { DEV = 0x1, TEST = 0x2 }

[GlobalClass]
public partial class RunEnvironment : Node {
  public static Environment Current { get; set; } = Environment.DEV;
}

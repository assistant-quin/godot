namespace Quin.Test;

using Godot;
using Chickensoft.GoDotTest;
using Shouldly;

public class PlayerTest : TestClass {
  public PlayerTest(Node scene) : base(scene) { }

  private readonly Avatar _nymph1 = (Avatar)ResourceLoader.Load("res://test/fixtures/nymph/nymph.avatar.tres");
  private readonly Avatar _nymph2 = (Avatar)ResourceLoader.Load("res://test/fixtures/nymph2/nymph2.avatar.tres");

  private readonly Player _player = new Globals().Player;

  [Test]
  public void TestPlayerTokens() {
    _player.Tokens += 100;
    _player.Tokens.ShouldBe(100);
  }

  [Test]
  public void TestPlayerShards() {
    _player.Shards += 100;
    _player.Shards.ShouldBe(100);
  }

  // cards
  [Test]
  public void TestPlayerCards() {
    _player.AddCard(_nymph1);
    _player.AddCard(_nymph1);
    _player.AddCard(_nymph2);
    _player.CardsCount(_nymph1).ShouldBe(2);
    _player.CardsCount(_nymph2).ShouldBe(1);
  }

  [Test]
  public void TestPlayerUnlockedAvatars() {
    _player.AddCard(_nymph1);
    _player.UpgradeAvatar(_nymph1).ShouldBe(true);
    _player.Avatar(_nymph1)!.Level.ShouldBe(1);

    _player.UpgradeAvatar(_nymph1).ShouldBe(false);
    _player.Avatar(_nymph1)!.Level.ShouldBe(1);

    _player.Avatar(_nymph2)!.ShouldBe(null);
  }

  [Test]
  public void TestAddAvatarExperience() {
    _player.AddCard(_nymph1);
    _player.UpgradeAvatar(_nymph1);

    _player.AddAvatarExperience(_nymph1, 100);
    _player.Avatar(_nymph1)!.Exp.ShouldBe(100);
  }
}

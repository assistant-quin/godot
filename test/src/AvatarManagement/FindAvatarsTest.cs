namespace Quin;

using Godot;
using Chickensoft.GoDotTest;
using Shouldly;

public class FindAvatarsTest : TestClass {
  public FindAvatarsTest(Node scene) : base(scene) { }

  private readonly Avatar _nymph1 = (Avatar)ResourceLoader.Load("res://test/fixtures/nymph/nymph.avatar.tres");
  private readonly Avatar _nymph2 = (Avatar)ResourceLoader.Load("res://test/fixtures/nymph2/nymph2.avatar.tres");

  [Test]
  public void TestItFindsTheEnvironmentAvatars() {
    var avatars = FindAvatars.Process(Environment.TEST);
    avatars.Contains(_nymph1).ShouldBeTrue();
    avatars.Contains(_nymph2).ShouldBeTrue();

    avatars = FindAvatars.Process(Environment.DEV);
    avatars.Contains(_nymph1).ShouldBeTrue();
    avatars.Contains(_nymph2).ShouldBeFalse();
  }
}

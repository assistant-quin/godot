namespace Quin;

using Godot;
using Chickensoft.GoDotTest;

public class TestClass : Chickensoft.GoDotTest.TestClass {
  public TestClass(Node testScene) : base(testScene) { }

  [SetupAll] public void SetupAll() => RunEnvironment.Current = Environment.TEST;
  [Setup] public void CleanPlayer() => new Globals().Player.Reset();
}

namespace Quin;

using Godot;
using Chickensoft.GoDotTest;
using Shouldly;
using Shouldly.ShouldlyExtensionMethods;

public class AvatarTest : TestClass {
  public AvatarTest(Node testScene) : base(testScene) { }

  private readonly Avatar _nymph = (Avatar)ResourceLoader.Load("res://test/fixtures/nymph/nymph.avatar.tres");

  [Test] public void TestAvatarName() => _nymph.Name.ShouldBe("Nymph");
  [Test] public void TestAvatarUid() => _nymph.Uid.ShouldBe("b61ea8b2-340b-4938-921f-24bfa089c759");

  [Test]
  public void TestAvatarScene() {
    var avatarScene = _nymph.AvatarScene!.Instantiate();
    avatarScene.ShouldBeAssignableTo<AvatarScene>();
    avatarScene.Free();
  }

  [Test]
  public void TestAvatarEnvironmentScene() {
    var scene = _nymph.EnvironmentScene!.Instantiate();
    scene.ShouldBeAssignableTo<AvatarEnvironmentScene>();
    scene.Free();
  }

  [Test]
  public void TestAvatarRunEnvironment() {
    _nymph.RunEnvironments.ShouldHaveFlag(Environment.DEV);
    _nymph.RunEnvironments.ShouldHaveFlag(Environment.TEST);
  }

  [Test]
  public void TestCardsPerLevel() {
    _nymph.Levels[1].Cards.ShouldBe(1);
    _nymph.Levels[2].Cards.ShouldBe(5);
  }

  [Test]
  public void TestExpPerLevel() {
    _nymph.Levels[1].Exp.ShouldBe(1000);
    _nymph.Levels[2].Exp.ShouldBe(5000);
  }
}

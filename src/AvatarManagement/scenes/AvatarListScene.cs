namespace Quin;

using Godot;

public partial class AvatarListScene : Control {
  public static readonly string SceneResourcePath = "res://src/AvatarManagement/scenes/AvatarList.tscn";

  public GridContainer CardsContainer { get; private set; } = default!;
  private static readonly PackedScene _cardPackedScene = (PackedScene)ResourceLoader.Load(AvatarCardScene.SceneResourcePath);

  public override void _Ready() {
    CardsContainer = GetNode<GridContainer>("%CardsContainer");

    foreach (var avatar in Globals.Avatars.Value) {
      var card = (AvatarCardScene)_cardPackedScene.Instantiate();
      CardsContainer.AddChild(card);
      card.Populate(avatar);
    }
  }
}

namespace Quin.Test;

using Godot;
using Chickensoft.GoDotTest;
using Shouldly;

public class AvatarLevelTest : TestClass {
  public AvatarLevelTest(Node testScene) : base(testScene) { }

  [Test]
  public void TestItHasCardsToNextLevel() {
    var level = new AvatarLevel();
    level.Cards.ShouldBe(0);
  }

  [Test]
  public void TestItHasExpToNextLevel() {
    var level = new AvatarLevel();
    level.Exp.ShouldBe(0);
  }
}


// { exp needed, cards needed }
